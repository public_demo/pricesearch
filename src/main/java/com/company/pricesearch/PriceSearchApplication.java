package com.company.pricesearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.company.pricesearch.infrastructure.persistence.repository")
public class PriceSearchApplication {

	public static void main(String[] args) {
		SpringApplication.run(PriceSearchApplication.class, args);
	}

}
