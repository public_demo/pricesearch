package com.company.pricesearch.infrastructure.api.rest;

import com.company.pricesearch.application.service.PriceSearchService;
import com.company.pricesearch.infrastructure.api.model.PriceDto;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

  @RestController
  @RequestMapping("/api")
  public class PriceSearchController implements ProductsApi {

    @Autowired
    private PriceSearchService priceSearchService;

    public ResponseEntity<PriceDto> getPriceByDateTime(@PathVariable Integer id, @RequestParam Integer brand,
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") @RequestParam LocalDateTime applicationDateTime) {
      return priceSearchService.getFirstPriceByDateTime(id, brand, applicationDateTime).map(ResponseEntity::ok)
          .orElseGet(() -> ResponseEntity.notFound().build());
    }
  }