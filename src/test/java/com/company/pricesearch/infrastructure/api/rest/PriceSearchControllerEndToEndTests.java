package com.company.pricesearch.infrastructure.api.rest;

import com.company.pricesearch.infrastructure.api.model.PriceDto;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PriceSearchControllerEndToEndTests {

  private static final int PRODUCT_ID = 35455;
  private static final int BRAND_ID = 1;
  private static final String HOST = "http://localhost:%s%s";
  private static final String PRICE_ENDPOINT = "/api/products/{id}/price?brand={brand}&applicationDateTime={applicationDateTime}";
  private static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
  private static final String DATE_TIME_BAD_FORMAT = "dd/MM/yyyy HH:mm:ss";

  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;

  @ParameterizedTest
  @CsvSource({
      "2020-06-14 10:00:00, 35.50",
      "2020-06-14 16:00:00, 25.45",
      "2020-06-14 21:00:00, 35.50",
      "2020-06-15 10:00:00, 30.50",
      "2020-06-16 21:00:00, 38.95"
  })
  public void should_ReturnCorrectPrice(String dateTime, String expectedPrice) {
    //Given

    //When
    ResponseEntity<PriceDto> response = restTemplate.getForEntity(
        String.format(HOST, port, PRICE_ENDPOINT),
        PriceDto.class,
        PRODUCT_ID,
        BRAND_ID,
        dateTime
    );

    PriceDto priceDto = response.getBody();

    //Then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isNotNull();

    assertThat(priceDto.getProductId()).isEqualTo(PRODUCT_ID);
    assertThat(priceDto.getBrandId()).isEqualTo(BRAND_ID);
    assertThat(priceDto.getPrice()).isEqualByComparingTo(new BigDecimal(expectedPrice));
    assertThat(priceDto.getCurr()).isEqualToIgnoringCase("EUR");
  }

  @Test
  public void should_ReturnNotFoundStatus_When_AskedForPriceAt21_00OnJune16() {

    //Given
    LocalDateTime applicationDateTime = LocalDateTime.of(2021, 12, 21, 21, 0);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);

    //When
    ResponseEntity<PriceDto> response = restTemplate.getForEntity(
        String.format(HOST, port, PRICE_ENDPOINT),
        PriceDto.class, PRODUCT_ID, BRAND_ID, formatter.format(applicationDateTime));

    //Then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
  }

  @Test
  public void should_ReturnBadRequestStatus_When_AskedForPriceWithDateTimeInBadFormat () {

    //Given
    LocalDateTime applicationDateTime = LocalDateTime.of(2021, 12, 21, 21, 0);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_BAD_FORMAT);

    //When
    ResponseEntity<PriceDto> response = restTemplate.getForEntity(
        String.format(HOST, port, PRICE_ENDPOINT),
        PriceDto.class, PRODUCT_ID, BRAND_ID, formatter.format(applicationDateTime));

    //Then
    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
  }

}
